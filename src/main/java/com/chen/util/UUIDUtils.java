package com.chen.util;

import java.util.UUID;

/**
 * Created wity IntelliJ IDEA
 *
 * @author chenhefu
 * Date 2019/3/25
 * Time 20:38
 */

/**
 * 生成UUID工具
 */
public class UUIDUtils {

    private static final ThreadLocal<String> threadId = ThreadLocal.withInitial(UUIDUtils::gen32UUID);

    public static String get() {
        return threadId.get();
    }

    public static void remove() {
        threadId.remove();
    }

    public static String gen32UUID() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
