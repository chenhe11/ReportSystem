package com.chen.util;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

/**
 * Created wity IntelliJ IDEA
 *
 * @author chenhefu
 * Date 2019/3/25
 * Time 20:46
 */
public class HashUtils {
    /**
     * 获取String的MD5值
     *
     * @param input 字符串
     * @return 该字符串的MD5值(32位)
     */
    public static String getMD5(String input) {
        if (input != null && !input.trim().isEmpty()) {
            try {
                MessageDigest md5 = MessageDigest.getInstance("MD5");
                md5.update(input.trim().getBytes(StandardCharsets.UTF_8));
                byte[] md5Array = md5.digest();
                return new BigInteger(1, md5Array).toString(16);
            } catch (Exception ignore) {
            }
        }
        return "";
    }
}
