package com.chen;

import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@MapperScan("com.chen.dao")
@SpringBootApplication
public class ReportSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReportSystemApplication.class, args);
    }

}
