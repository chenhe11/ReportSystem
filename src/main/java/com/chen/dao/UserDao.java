package com.chen.dao;

import com.chen.model.User;

import java.util.List;

/**
 * Created wity IntelliJ IDEA
 *
 * @author chenhefu
 * Date 2019/3/25
 * Time 16:46
 */
public interface UserDao {
    public User getUser(Long id);
    public void deleteUser(Long id);
    List<User> getAllUser();
}
