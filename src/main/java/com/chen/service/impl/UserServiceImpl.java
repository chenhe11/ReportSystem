package com.chen.service.impl;

import com.chen.dao.UserDao;
import com.chen.model.User;
import com.chen.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created wity IntelliJ IDEA
 *
 * @author chenhefu
 * Date 2019/3/25
 * Time 16:50
 */
@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;
    @Override
    public User getUserById(Long id) {
        return userDao.getUser(id);
    }
}
