package com.chen.service;

import com.chen.model.User;

/**
 * Created wity IntelliJ IDEA
 *
 * @author chenhefu
 * Date 2019/3/25
 * Time 16:49
 */
public interface UserService {
    public User getUserById(Long id);
}
