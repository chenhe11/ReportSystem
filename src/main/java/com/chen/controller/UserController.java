package com.chen.controller;

import com.chen.model.User;
import com.chen.service.UserService;
import com.chen.util.JsonResult;
import com.chen.util.Response;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created wity IntelliJ IDEA
 *
 * @author chenhefu
 * Date 2019/3/25
 * Time 16:52
 */
@RestController
@RequestMapping("/user")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    UserService userService;

    @RequestMapping("/get")
    public JsonResult getUserById(Long id){
         User user =  userService.getUserById(id);
         //logger.info("id = "+user.getUsername());
         //Response response = new Response();
        JsonResult jsonResult = new JsonResult();
         if (user!=null){
             jsonResult.setData(user);
             jsonResult.setMessage("sucess");
             jsonResult.setState(0);
         }else {
             jsonResult.setState(1);
             jsonResult.setMessage("failure");
         }
         return jsonResult;
    }
}
